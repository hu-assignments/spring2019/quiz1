#include <stdio.h>
#include <stdlib.h>

void print_tribonacci(int n, int *trib_arr_ptr);
void free_mem(int *ptr);

int main()
{
    int number_of_elements;
    printf("Enter a number: ");
    scanf("%d", &number_of_elements);

    // Allocating memory for Tribonacci number array.
    int *trib_arr = (int *)malloc(number_of_elements * sizeof(int));

    // Placing first three elements of Tribonacci series.
    // For not accessing the memory the program should not access,
    // those if statements are needed.
    if (number_of_elements >= 1)
        *trib_arr = 0;
    if (number_of_elements >= 2)
        *(trib_arr + 1) = 1;
    if (number_of_elements >= 3)
        *(trib_arr + 2) = 2;

    int i;
    for (i = 3; i < number_of_elements; i++)
    {
        *(trib_arr + i) = *(trib_arr + i - 1) + *(trib_arr + i - 2) + *(trib_arr + i - 3);
    }

    print_tribonacci(number_of_elements, trib_arr);
    free_mem(trib_arr);
    return 0;
}

void print_tribonacci(int n, int* trib_arr_ptr)
{
    int i;
    for (i = 0; i < n; i++)
    {
        printf((i == n - 1) ? "%d" : "%d ", *(trib_arr_ptr + i));
    }
}

void free_mem(int *ptr)
{
    free(ptr);
    ptr = NULL;
}